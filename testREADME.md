# Installation

cd to your folder
```
git clone git@bitbucket.org:gfl-internal/strapi-backend.git
cd strapi-backend
npm install
```

# Database configuration

Default database in strapi is sqlite.
To configure another database, need to use *config/database.js*

#### Example 
*config/database.js* for sqlite
```js
module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'sqlite',
        filename: env('DATABASE_FILENAME', '.tmp/data.db'),
      },
      options: {
        useNullAsDefault: true,
      },
    },
  },
});
```
*config/database.js* for mongodb
```js
module.exports = ({ env }) => ({
  "defaultConnection": "default",
  "connections": {
    "default": {
      "connector": "mongoose",
      "settings": {
        "uri": "mongodb://paulbocuse:<password>@strapidatabase-shard-00-00-fxxx6c.mongodb.net:27017,strapidatabase-shard-00-01-fxxxc.mongodb.net:27017,strapidatabase-shard-00-02-fxxxc.mongodb.net:27017/test?ssl=true&replicaSet=strapidatabase-shard-0&authSource=admin&retryWrites=true&w=majority"
      },
      "options": {
        "ssl": true
      }
    }
  }
});
```
Uri can be used for atlas mongodb as example *uri*: 'mongodb+srv://admin:password@posts.ciowm.mongodb.net/posts?retryWrites=true&w=majority' 

# Statistics feature

*config/api-config.js* configurations
#### Usage example
```js
// The statistics collection
'post-stats': { // Uid
  post_id: {type: 'integer'},
  posts_comments_len: {type: 'integer'} // Ended <_len>, if not exist, will be created
}

// The Posts collection
Posts: {
  id: {type: 'integer'}
  post_comments: {type: 'relation'}
}

// config/api-config.js
{
  'post-stats': { // The collection uid for statistics
    relationName: 'posts', // The name(or alias) of the collection to be saved to statistics.
    counterRelationField: 'post_id' // The 'relation' id field between collections
  }
}
```
*api/services.js* return { updateCounter }

```js
async function updateCounter(
  collectionId,
  counterCollectionUid, 
  relationField, 
  isIncrement = true) // Increment or decrement counter
  {...}
```

*api/post-comments/controllers/post-comments.js*

Strapi controllers 
[documentation](https://strapi.io/documentation/v3.x/concepts/controllers.html#concept)
```js
const { updateCounter } = require('../../services');

module.exports = {
  async create(ctx) {
    const body = ctx.request.body;
    const comment = await strapi.services['post-comments'].create(body);
    await updateCounter(body.postId, // The Posts collection id
      'post-stats', // The statistics collection uid
      'post_comments'); // The relation to be counted

    return comment;
  },
}
```